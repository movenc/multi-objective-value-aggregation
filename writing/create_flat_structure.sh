rm -r flat_structure/*
cp main-aamas2021.tex flat_structure/
cp custom_frontmatter.tex flat_structure/
cp aamas.cls flat_structure/
cp mainbib.bib flat_structure/
cp zoterogenerated.bib flat_structure/
cp ACM-Reference-Format.bst flat_structure/
cp sections/*.tex flat_structure/
cp ../output/*.pdf flat_structure/
cp ../output/*.png flat_structure/
cp ../output/*.tex flat_structure/
cp ../output/pymove_graphs/*.pdf flat_structure/
cp ../output/pymove_tables/*.tex flat_structure/
cp ../output/tables/*.tex flat_structure/
sed -i '' 's/writing\/sections\///' flat_structure/main-aamas2021.tex
sed -i '' '/{/s/output\/pymove_graphs\///g' flat_structure/*.tex
sed -i '' '/{/s/output\/pymove_tables\///g' flat_structure/*.tex
sed -i '' '/{/s/output\/tables\///g' flat_structure/*.tex
sed -i '' '/{/s/output\///g' flat_structure/*.tex

#rename a bunch of files whose names are too long
#first replace references to them
#this is a bit tricky; my regex below isn't sophisticated so
#we are replacing ALL string matches referring to these files
sed -i '' 's/default_scale_progress/dsp/' flat_structure/*.tex
sed -i '' 's/pilot_granularity_tuned_selected/pgts/g' flat_structure/*.tex
sed -i '' 's/cumulative_granularity_progress_by_agent/cgpba/g' flat_structure/*.tex
sed -i '' 's/granularity_progress/gp/g' flat_structure/*.tex
sed -i '' '/{/s/multirun_n100/mrn100/g' flat_structure/*.tex
#now replace the files themselves
cd flat_structure
rename 's/default_scale_progress/dsp/g' *.pdf
rename 's/pilot_granularity_tuned_selected/pgts/g' *.pdf
rename 's/cumulative_granularity_progress_by_agent/cgpba/g' *.pdf
rename 's/granularity_progress/gp/g' *.pdf
rename 's/multirun_n100/mrn100/g' *.pdf
cd ../
#echo "the work is not done! when you upload this to JAMAAS, including any text files not in the original will givey ou all kinds of hell. don't do it.'"

#now we're going to tidy up the images. only include the images we want
mkdir flat_structure/figures/
ack -s '(?!\%.*)(?=.*)(?={)(.*(\.pdf|\.png|.jpg))(?=)}' flat_structure/*.tex
#now use the list to move
ack -s '(?!\%.*)(?=.*)(?={)(.*(\.pdf|\.png|.jpg))(?=)}' flat_structure/*.tex -oh | sed -E 's/({|})//g' | sed -E 's/^/flat_structure\//g' | xargs -I '{}' mv {} flat_structure/figures/
#remove the rest
rm flat_structure/*.png
rm flat_structure/*.pdf
rm flat_structure/*.jpg
mv flat_structure/figures/* flat_structure/
rm -r flat_structure/figures
echo "warning: probably included figures from a deleted sction that we need to remove."