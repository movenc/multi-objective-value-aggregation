#search string to identify all files 
#\n^(?!.*\%.*).*(\.pdf|\.png|.jpg)}
#alternatively:
ack -s '(?!\%.*).*(\.pdf|\.png|.jpg)}' *.tex
#alternatively
ack -s '(?!\%.*)(?=.*)\{(.*(\.pdf|\.png|.jpg))\}' flat_structure/*.tex -oh > matches.txt
#alternatively just do it all in one command!
#print
ack -s '(?!\%.*)(?=.*)(?={)(.*(\.pdf|\.png|.jpg))(?=)}' flat_structure/*.tex
#now use the list to move
ack -s '(?!\%.*)(?=.*)(?={)(.*(\.pdf|\.png|.jpg))(?=)}' flat_structure/*.tex -oh | sed -E 's/({|})//g' | sed -E 's/^/flat_structure\//g' | xargs -I '{}' mv {} flat_structure/figures/
#remove the rest
rm flat_structure/*.png
rm flat_structure/*.pdf
rm flat_structure/*.jpg