import numpy as np
import matplotlib.pyplot as plt

def sflla(x):
    if x > 0:
        return np.log(x + 1)
    else:
        return -np.exp(-x) + 1

sflla_vec = np.vectorize(sflla)

xs = np.linspace(-1, 1, 100)

for c in [0.1, 1, 10]:
    plt.plot(1*xs, sflla_vec(c*xs), label=f"c={c}")
plt.legend()
plt.ylim(-5, 5)
plt.grid()
plt.show()
